const student = {
	group: "PZ-25",
	name: "Roman P",
	gender: "M",
	birthday: "01.01.1999",
	status: "active"
};

const configureDeleteButton = (button) => {
	button.addEventListener("click", () => {
		const row = button.closest("tr");
		const nameCell = row.querySelector(".name-cell");

		const modal = document.querySelector("#delete-student-modal");
		const modalBody = document.querySelector("#delete-student-modal .modal-body")
		const approveButton = document.querySelector("#delete-student-modal .approve");
		const cancelButton = document.querySelector("#delete-student-modal .cancel");
		const closeButton = document.querySelector("#delete-student-modal .close");

		modalBody.textContent += nameCell.textContent + "?";
		modal.classList.add("show");

		approveButton.addEventListener("click", () => {
			row.remove();
			modal.classList.remove("show");
			modalBody.textContent = modalBody.textContent.replace(nameCell.textContent + '?', '');
		});

		cancelButton.addEventListener("click", () => {
			modal.classList.remove("show");
			modalBody.textContent = modalBody.textContent.replace(nameCell.textContent + '?', '');
		});
		closeButton.addEventListener("click", () => {
			modal.classList.remove("show");
			modalBody.textContent = modalBody.textContent.replace(nameCell.textContent + '?', '');
		});
	});
}

const configureOptions = () => {
	const deleteButtons = document.querySelectorAll(".delete-btn");
	deleteButtons.forEach((button) => {
		configureDeleteButton(button);
	});
}

const setAddModalBody = (student, modal, toClear) => {
	const groupInput = modal.querySelector(".group-input");
	const nameInput = modal.querySelector(".name-input");
	const genderInput = modal.querySelector(".gender-input");
	const birthdayInput = modal.querySelector(".birthday-input");
	const statusInput = modal.querySelector(".status-input");

	if(toClear){
		groupInput.textContent = groupInput.textContent.replace(student.group, '');
		nameInput.textContent = nameInput.textContent.replace(student.name, '');
		genderInput.textContent = genderInput.textContent.replace(student.gender, '');
		birthdayInput.textContent = birthdayInput.textContent.replace(student.birthday, '');
		statusInput.textContent = statusInput.textContent.replace(student.status === "active" ? "Active" : "Inactive", '');
	} else{
		groupInput.textContent += student.group;
		nameInput.textContent += student.name;
		genderInput.textContent += student.gender;
		birthdayInput.textContent += student.birthday;
		statusInput.textContent += student.status === "active" ? "Active" : "Inactive";
	}
}

const createStudentRow = (student) => {
	return `		
	<td class="checkbox-cell">
		<input type="checkbox"/>
	</td>
	<td class="group-cell">${student.group}</td>
	<td class="name-cell">${student.name}</td>
	<td class="gender-cell">${student.gender}</td>
	<td class="birthday-cell">${student.birthday}</td>
	<td><span class="status-cell ${student.status === "active" ? "active-status" : "inactive-status"}"></span></td>
		<td class="options-cell">
		<button><img src="./img/edit.svg" alt="edit" /></button
		><button class="delete-btn">
			<img src="./img/delete.svg" alt="delete" />
		</button>
	</td>
`;
};

const configureAddButton = () => {
	const addButton = document.querySelector(".add-student-btn");
	const addModal = document.querySelector("#add-student-modal");
	const closeModalBtn = addModal.querySelector(".close");
	const createStudentBtn = addModal.querySelector(".create");

	addButton.addEventListener("click", () => {
		addModal.style.display = "block";
		setAddModalBody(student, addModal, false);
	});
	closeModalBtn.addEventListener("click", () => {
		setAddModalBody(student, addModal, true);
		addModal.style.display = "none";
	});
	
	createStudentBtn.addEventListener("click", () => {
		setAddModalBody(student, addModal, true);
		const table = document.querySelector(".table");
		const row = table.insertRow();
		row.innerHTML = createStudentRow(student);

		const deleteButton = row.querySelector('.delete-btn');
		addModal.style.display = "none";
		
		configureDeleteButton(deleteButton);
	});
};

const notification = document.querySelector('.notification-logo ')
notification.addEventListener('pointerenter', () => {
	notification.classList.remove('notification-animation');
	notification.offsetWidth;
	notification.classList.add('notification-animation');
})

configureAddButton();
configureOptions();

